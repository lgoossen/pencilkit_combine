//
//  Model.swift
//  PencilKit+Combine
//
//  Created by Lucas Goossen on 6/10/20.
//  Copyright © 2020 Lucas Goossen. All rights reserved.
//

import Foundation
import Combine
import AVFoundation

class Model:ObservableObject {
    var nonCombineScale:CGFloat = 1
    @Published var combineScale:CGFloat = 1
}
