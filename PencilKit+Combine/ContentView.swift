//
//  ContentView.swift
//  PencilKit+Combine
//
//  Created by Lucas Goossen on 6/10/20.
//  Copyright © 2020 Lucas Goossen. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var model:Model
    var body: some View {
        PencilView()
    }
}
//  Pencil.swift
//  Created by Lucas Goossen on 6/5/20.
import Foundation
import SwiftUI
import PencilKit
import Combine

struct PencilView : UIViewRepresentable {
    @EnvironmentObject var model:Model
    let canvasView = PKCanvasView()
    let delegate = PencilViewDelegate()
    let coordinator = Coordinator()
    
    class Coordinator: NSObject, PKToolPickerObserver {
        var color = UIColor.black
        var thickness = CGFloat(30)
        
        func toolPickerSelectedToolDidChange(_ toolPicker: PKToolPicker) {
            if toolPicker.selectedTool is PKInkingTool {
                let tool = toolPicker.selectedTool as! PKInkingTool
                self.color = tool.color
                self.thickness = tool.width
            }
        }
        func toolPickerVisibilityDidChange(_ toolPicker: PKToolPicker) {
            if toolPicker.selectedTool is PKInkingTool {
                let tool = toolPicker.selectedTool as! PKInkingTool
                self.color = tool.color
                self.thickness = tool.width
            }
        }
    }
    func makeCoordinator() -> PencilView.Coordinator {
        return Coordinator()
    }
    func makeUIView(context: Context) -> PKCanvasView {
        let player = UILabel()
        player.text = "Hello Broken"
        player.frame = canvasView.subviews.first!.frame//CGRect(x: 0, y: 0, width: 1024, height: 600)
        canvasView.subviews.first?.addSubview(player)
        player.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        canvasView.subviews.first?.sendSubviewToBack(player)
        canvasView.isOpaque = false
        canvasView.becomeFirstResponder()
        canvasView.maximumZoomScale = 4
        canvasView.contentScaleFactor = 4
        delegate.model = model
        canvasView.delegate = delegate
        if let window = UIApplication.shared.windows.filter({$0.isKeyWindow}).first,
            let toolPicker = PKToolPicker.shared(for: window) {
            toolPicker.addObserver(canvasView)
            toolPicker.addObserver(coordinator)
            toolPicker.setVisible(true, forFirstResponder: canvasView)
            toolPicker.overrideUserInterfaceStyle = .light
        }
        return canvasView
    }
    func updateUIView(_ uiView: PKCanvasView, context: Context) {}
}

class PencilViewDelegate:NSObject, PKCanvasViewDelegate {
    var model:Model?
    @Published var testvar:CGFloat = 1
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        //        This give me updates while scrolling. May use to make projector follow more closely
        print(scrollView.zoomScale)
//        model?.docCamZoom = scrollView.zoomScale
        model?.combineScale = scrollView.zoomScale

    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        // This gives me only the final result once finger lifed.
//        model?.test = scrollView.zoomScale
        print(scale)
//        model?.docCamZoom = scrollView.zoomScale
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset)
//        model?.docCamZoomAnchor = UnitPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y)
    }
    func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        print("CHANGE")
        model?.nonCombineScale = canvasView.zoomScale
        //        canvasView.sendSubviewToBack(canvasView.subviews.first!)
    }
}
